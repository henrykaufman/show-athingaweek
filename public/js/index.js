var app = angular.module('app', []);

app.controller('HomeCtrl', ['$scope', '$http', function($scope, $http) {

}]);

app.controller('NewCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.submitPost = function(post) {
        var data = {
            name: post.name,
            desc: post.desc,
            email: post.email,
            username: post.username,
            url: post.url
        }
        console.log(data);
        $http.post('/api/newPost', data)
        .success(function(data) {
            post.name = '';
            post.desc = '';
            post.email = '';
            post.username = '';
            post.url = '';
            $scope.response = data;
            console.log(data);
        })
    }
}]);

app.controller('AdminCtrl', ['$scope', '$http', function($scope, $http) {
    $scope.approve = function(post) {
        var data = {
            id: post._id,
            number: $scope.week.number
        }
        $http.post('/api/approve', data)
        .success(function(data) {
            console.log(data);
        });
    }
    $scope.newTheme = function(theme) {
        var data = {
            theme: theme.theme,
            number: theme.number
        }
        console.log(data);
        $http.post('/api/newTheme', data)
        .success(function(data) {
            console.log(data);
        });
    }
}]);
