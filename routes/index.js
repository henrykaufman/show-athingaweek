var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
var router = express.Router();

password = '';

Date.prototype.getWeek = function() {
    var onejan = new Date(this.getFullYear(), 0, 1);
    return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
}

var conn = mongoose.createConnection('mongodb://localhost/show-athingaweek-v1');

var WeekSchema = new mongoose.Schema({
    number: String,
    theme: String,
    posts: [{
        name: String,
        desc: String,
        username: String,
        email: String,
        url: String,
        approved: Boolean
    }]
});

var Week = conn.model('Week', WeekSchema);

router.get('/', function(req, res, next) {
    Week.find({}, function(err, weeks) {
        if (weeks) {
            if (weeks.length == 0 || typeof weeks == 'undefined') {
                res.render('index', {
                    title: 'Show - A Thing A Week'
                });
            } else {
                for (var i = 0; i < weeks.length; i++) {
                    if (weeks[i].number == ((new Date).getWeek() + 1).toString()) {
                        console.log(weeks[i]);
                        res.render('index', {
                            title: 'Show - A Thing A Week',
                            week: weeks[i]
                        });
                    }
                }
            }
        } else {
            res.render('index', {
                title: 'Show - A Thing A Week'
            });
        }
    });
});

router.get('/new', function(req, res) {
    res.render('new', {
        title: 'New Post'
    });
});

router.get('/login', function(req, res) {
    res.render('login', {
        title: 'Login'
    });
});

router.post('/api/newPost', function(req, res) {
    Week.findOne({
        number: (new Date).getWeek() + 1
    }, function(err, week) {
        if (week) {
            console.log(req.body);
            var data = {
                name: req.body.name,
                desc: req.body.desc,
                url: req.body.url,
                username: req.body.username,
                email: req.body.email,
                approved: false
            }
            week.posts.push(data);
            week.save(function(err, week) {
                if (err) return console.log(err);
                console.log(week);
                res.send('Looks good! We will approve it soon!');
            });
        }
    });
});

router.post('/api/approve', function(req, res) {
    bcrypt.compare('GavinandHenry', bcrypt.hashSync(password), function(err, response) {
        if (response == true) {
            Week.findOne({
                number: req.body.number
            }, function(err, week) {
                if (week) {
                    for (var i = 0; i < week.posts.length; i++) {
                        if (week.posts[i]._id == req.body.id) {
                            week.posts[i].approved = true;
                            week.save(function(err, week) {
                                if (err) return console.log(err);
                                console.log(week);
                                res.send(week);
                            });
                        }
                    }
                }
            });
        } else {
            res.send('no good');
        }
    });
});

router.post('/api/newTheme', function(req, res) {
    bcrypt.compare('GavinandHenry', bcrypt.hashSync(password), function(err, response) {
        if (response == true) {
            var newWeek = new Week({
                theme: req.body.theme,
                number: req.body.number
            });
            newWeek.save(function(err, week) {
                if (err) return console.log(err);
                console.log(week);
                res.send(week);
            });
        } else {
            res.send('no good');
        }
    });
});

router.post('/login', function(req, res) {
    console.log(req.body.password);
    bcrypt.compare('GavinandHenry', bcrypt.hashSync(req.body.password), function(err, response) {
        if (response == true) {
            password = req.body.password;
            Week.find({}, function(err, weeks) {
                console.log(weeks);
                if (weeks) {
                    if (weeks.length == 0 || typeof weeks == 'undefined') {
                        res.render('admin', {
                            title: 'Admin'
                        });
                    } else {
                        for (var i = 0; i < weeks.length; i++) {
                            console.log(weeks[i].number);
                            console.log(((new Date).getWeek() + 1).toString());
                            if (weeks[i].number == ((new Date).getWeek() + 1).toString()) {
                                console.log(weeks[i]);
                                res.render('admin', {
                                    title: 'Admin',
                                    week: weeks[i]
                                });
                            }
                        }
                    }
                } else {
                    res.render('admin', {
                        title: 'Admin'
                    });
                }
            });
        } else {
            res.send('no good');
        }
    });
});

module.exports = router;
